const https = require('https');

var options = {
    "method": "GET",
    "hostname": "rest.coinapi.io",
    "path": "/v1/assets",
    "headers": {'X-CoinAPI-Key': '73034021-THIS-IS-SAMPLE-KEY'}
  };

var chunks = [];
var request = https.request(options, function (response) {
  response.on("data", function (chunk) {
    chunks.push(chunk);
  });
});

request.end();
console.log(chunks);