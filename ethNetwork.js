const  { tx } = require('@binance-chain/bcw');
const Web3 = require('web3');
// // mainnet
// const web3 = new Web3('https://bsc-dataseed1.binance.org:443');

// testnet
const web3 = new Web3('http://localhost:8545');
const add_1 = "0xcF29f6D9C0b9E0F7169c2215c631066aB42E0181";
const add_2 = "0x52E66b8cE7d016C87b22fCbe1606c857781EDDf0";

// const PRIVATE_KEY = "dc0b16badc30e40393ba0479ea2d56e542d8530a87167254a5a8232aaa3980ec";
const PRIVATE_KEY = "c20be2abc994a265a668010d67bc05a4d93a3236e26cb2f4b39caa40d8b7b866";
const BSC_BNB_DATA = {
  from: add_1,
  to: add_2,
  value: '0.1',
  network: {
    id: 'ropsten-testnet',
    url: 'http://localhost:8545',
  },
  asset: {
    networkSymbol: 'ETH',
    decimals: 18,
  },
};

async function test () {

    const { sign, fee, send } = await tx.prepareToSendTx(BSC_BNB_DATA);

    web3.eth.getBalance(add_1).then(value=>console.log("address 1", value));
    console.log("fee", fee)
    // Before send transaction, please make sure YourBalance > fee + BSC_BNB_DATA.value.
    
    const result = await send(sign(PRIVATE_KEY));

console.log(result);

web3.eth.getBalance(add_2).then(value=>console.log("address 2", value));
}

// web3.eth.getBalance(add_1).then(value=>console.log("address 1", value));
// web3.eth.getBalance(add_2).then(value=>console.log("address 2", value));
test()
