
const axios = require('axios').default;
const Web3 = require('web3');
const web3 = new Web3("https://bsc-dataseed1.binance.org");


async function req(url) {
    const response = await axios.get(url)
    return response.data
}

async function request(){
      //bscscan
      //request API key for bscscan https://bscscan.com/myapikey 
    //Tip : The result is returned in the token's smallest decimal representation(wei).
    //Eg. a token with a balance of 215.241526476136819398 and 18 decimal places will be returned as 215241526476136819398
    let bsctokenSupply = await req('https://api.bscscan.com/api?module=stats&action=tokensupply&contractaddress=0x2170ed0880ac9a755fd29b2688956bd959f933f8&apikey=19U6MNFU88HAI36SANNBA3ZHWF3SNZVJES');
    bsctokenSupply = web3.utils.fromWei(bsctokenSupply['result']);
    console.log("bsctokenSupply ",bsctokenSupply);

    let bsctokenCSupply = await req('https://api.bscscan.com/api?module=stats&action=tokenCsupply&contractaddress=0x2170ed0880ac9a755fd29b2688956bd959f933f8&apikey=19U6MNFU88HAI36SANNBA3ZHWF3SNZVJES');
    bsctokenCSupply = web3.utils.fromWei(bsctokenCSupply['result']);
    console.log("bsctokenCSupply ",bsctokenCSupply);

    //get balance by user address
    let bsctokenBalance = await req('https://api.bscscan.com/api?module=account&action=tokenbalance&contractaddress=0x2170ed0880ac9a755fd29b2688956bd959f933f8&address=0x3c6a3acc7e6c93d238b27cd4a693d8fb453fa34a&tag=latest&apikey=19U6MNFU88HAI36SANNBA3ZHWF3SNZVJES');
    bsctokenBalance = web3.utils.fromWei(bsctokenBalance['result']);
    console.log(" get bsctokenBalance by add ",bsctokenBalance);
    //bscscan

    //pancakeswap
    let tokenInfo = await req('https://api.pancakeswap.info/api/v2/tokens/0x4b74e109e8b0d03226224ef2746f8c13e4c59cd4');
    console.log("pankack tokenInfo ",tokenInfo);
    //pancakeswap

    //etherscan
        let ethtokenSupply = await req(`https://api.etherscan.io/api?module=stats&action=tokensupply&contractaddress=0xB8c77482e45F1F44dE1745F52C74426C631bDD52&apikey=ASMZSK3B5FVS3P5P6H9B6TEVCDBR1GGSJH`);
        ethtokenSupply = web3.utils.fromWei(ethtokenSupply['result']);
        console.log("ethtokenSupply ",ethtokenSupply);
        let ethtokenaddSupply = await req(`https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=0xB8c77482e45F1F44dE1745F52C74426C631bDD52&address=0xb4b3351918a9bedc7d386c6a685c42e69920b34d&tag=latest&apikey=ASMZSK3B5FVS3P5P6H9B6TEVCDBR1GGSJH`);
        ethtokenaddSupply = web3.utils.fromWei(ethtokenaddSupply['result']);
        console.log(" get ethtokenSupply by add ",ethtokenaddSupply);
    //etherscan
}

  

request()