const solanaWeb3 = require("@solana/web3.js");
const bip39 = require("bip39");
const ed25519 = require("ed25519-hd-key");
const solana = new solanaWeb3.Connection("https://api.testnet.solana.com");
const crypto = require("crypto");
const derivePath = "m/44'/501'/0'/0'";

generate = async () => {
  const recentBlock = await solana.getEpochInfo();
  console.log(recentBlock);

  const phrase = bip39.generateMnemonic();
  console.log(phrase)
  const seed = bip39.mnemonicToSeed(phrase);
  const derivedSeed = ed25519.derivePath(
    derivePath,
    (await seed).toString("hex")
  ).key;

  const keypair = solanaWeb3.Keypair.fromSeed(derivedSeed);
  const publicKey = keypair.publicKey.toString();
  console.log(publicKey)

//   let programAddressFromKey = await solanaWeb3.PublicKey.createProgramAddress([derivedSeed], keypair.publicKey);
//   console.log(`Generated Program Address: ${programAddressFromKey.toBase58()}`);
};

generate();
