const  { tx } = require('@binance-chain/bcw');
const Web3 = require('web3');
const ethers = require("ethers");
// mainnet
// const web3 = new Web3('https://bsc-dataseed1.binance.org:443');

// testnet
const web3 = new Web3('https://data-seed-prebsc-1-s1.binance.org:8545');
const add_1 = "0x3792cfafeb1c6b572bf360a4ca9Db237CEe74156";
const add_2 = "0xE09b2dA6F270bDEfE7b88D4eD47909186e7D4e2e";

const PRIVATE_KEY = "dc0b16badc30e40393ba0479ea2d56e542d8530a87167254a5a8232aaa3980ec";
const BSC_BNB_DATA = {
  from: add_1,
  to: add_2,
  value: '0.2',
  network: {
    id: 'bsc-testnet',
    url: 'https://data-seed-prebsc-2-s2.binance.org:8545',
  },
  asset: {
    networkSymbol: 'BNB',
    decimals: 18,
  },
};

async function test () {

    const { sign, fee, send } = await tx.prepareToSendTx(BSC_BNB_DATA);

    web3.eth.getBalance(add_1).then(value=>console.log("address 1", value));
    console.log("fee", fee)
    // Before send transaction, please make sure YourBalance > fee + BSC_BNB_DATA.value.
    const result = await send(sign(PRIVATE_KEY));

console.log(result);

web3.eth.getBalance(add_2).then(value=>console.log("address 2", value));
}

web3.eth.getBalance(add_1).then(value=>console.log("address 1", value));
web3.eth.getBalance(add_2).then(value=>console.log("address 2", value));
// test()
