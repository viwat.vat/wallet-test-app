main = async () => {
  const network = "https://api.testnet.solana.com";
  const connection = new Connection(network);
  const blockhash = (await connnection.getRecentBlockhash("finalized"))
    .blockhash;
  const instruction = SystemProgram.transfer({
    fromPubkey: new PublicKey(publicKeys[0]),
    toPubkey: new PublicKey(publicKeys[1]),
    lamports: 0.1 * LAMPORTS_PER_SOL,
  });

  const transaction = new Transaction({
    recentBlockhash: blockhash,
    feePayer: new PublicKey(publicKeys[0]),
  }).add(TransactionInstruction);
  const transaction2 = new Transaction();

  const signedTransaction = await torus.signTransaction(transaction);
  const signature = await connection.sendRawTransaction(
    signedTransaction.serialize()
  );
};
