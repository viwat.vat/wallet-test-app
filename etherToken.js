const Web3 = require("web3");
// https://eth-ropsten.alchemyapi.io/v2/E6hFktNQl6uPhf2_U_C_ReZwhCq76fKC
const web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://ropsten.infura.io/v3/0d6c797dafe5405f8470c2056635b039"
  )
);
const ethers = require("ethers");
const abi = require("./abi.json");

main = async () => {
  let contractAddress = "0xe09ba5399ebe54b5c5537b0dbc78c673a7679c74";
  if (contractAddress) {
    let contract = new web3.eth.Contract(abi, contractAddress);
    const nonce = await web3.eth.getTransactionCount(
      "0xcF29f6D9C0b9E0F7169c2215c631066aB42E0181",
      "latest"
    );
    // const value = ethers.utils.parseEther("9990000000000000", 18).toHexString();
    const balance = await contract.methods.balanceOf('0xcF29f6D9C0b9E0F7169c2215c631066aB42E0181').call();
    const gasPrice = await web3.eth.getGasPrice();
    const gasLimit = ethers.utils.hexlify(0x21000)

    const transaction = {
      nonce,
      from: "0xcF29f6D9C0b9E0F7169c2215c631066aB42E0181",
      to: "0xe09ba5399ebe54b5c5537b0dbc78c673a7679c74",
      gasPrice,
      gasLimit,
      // value,
      data: contract.methods.transfer("0xA878fDB740f1C0BBAf3ab6afDc210f1d8A0bbcf6", 8000000000000000).encodeABI(),
    };

    const signedTx = await web3.eth.accounts.signTransaction(
      transaction,
      "c20be2abc994a265a668010d67bc05a4d93a3236e26cb2f4b39caa40d8b7b866"
    );

    web3.eth.sendSignedTransaction(
      signedTx.rawTransaction,
      function (error, hash) {
        if (!error) {
          console.log("🎉 The hash of your transaction is:", hash);
        } else {
          console.log(
            "❗Something went wrong while submitting your transaction:",
            error
          );
        }
      }
    );
    // try {
    //   await contract.methods
    //     .transfer("0xA878fDB740f1C0BBAf3ab6afDc210f1d8A0bbcf6", value)
    //     .send({ from: "0xcF29f6D9C0b9E0F7169c2215c631066aB42E0181" });
    // } catch (error) {
    //   console.log(error);
    // }
  }
};

main();
