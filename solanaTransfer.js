const web3 = require("@solana/web3.js");
const solana = new web3.Connection("https://api.testnet.solana.com");
const ed25519 = require("ed25519-hd-key");
const bip39 = require("bip39");
const derivePath = "m/44'/501'/0'";

sendTransaction = async () => {
  const phraseK =
    "setup chef select project vessel bomb silly divorce dumb pet enable circle";
  const seedK = bip39.mnemonicToSeed(phraseK);
  const derivedSeedK = ed25519.derivePath(
    derivePath,
    (await seedK).toString("hex")
  ).key;
  const to = web3.Keypair.fromSeed(derivedSeedK);

  //payer

  const phraseP =
    "appear spot strategy scheme more sure episode quantum option deal chief seminar";
  const seedP = bip39.mnemonicToSeed(phraseP);
  const derivedSeedP = ed25519.derivePath(
    derivePath,
    (await seedP).toString("hex")
  ).key;

  const from = web3.Keypair.fromSeed(derivedSeedP);

  let connection = new web3.Connection(web3.clusterApiUrl("testnet"));
  let transaction = new web3.Transaction().add(
    web3.SystemProgram.transfer({
      fromPubkey: from.publicKey,
      toPubkey: to.publicKey,
      lamports: web3.LAMPORTS_PER_SOL / 10,
    })
  );

  console.log(transaction);
  // Sign transaction, broadcast, and confirm
  let signature = await web3.sendAndConfirmTransaction(
    connection,
    transaction,
    [from]
  );
  console.log("SIGNATURE", signature);
  console.log("SUCCESS");
};

sendTransaction();
