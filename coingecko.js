//1. Import coingecko-api
const CoinGecko = require('coingecko-api');

//2. Initiate the CoinGecko API Client
const CoinGeckoClient = new CoinGecko();

//3. Make calls
main = async() => {
    // let data = await CoinGeckoClient.global();
    // let data = await CoinGeckoClient.coins.list();
    let data = await CoinGeckoClient.coins.fetch('bitcoin', {});
  console.log(data);
};

main();