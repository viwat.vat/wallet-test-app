const abiArray = require("./busd-abi.json");
const web3 = require("web3");
// const web3 = new Web3(
//   new Web3.providers.HttpProvider(
//     "https://mainnet.infura.io/v3/0d6c797dafe5405f8470c2056635b039"
//   )
// );
const myAddress = "0xA878fDB740f1C0BBAf3ab6afDc210f1d8A0bbcf6";
const toAddress = "0xcF29f6D9C0b9E0F7169c2215c631066aB42E0181";

const { Transaction } = require("@ethereumjs/tx");
const Common = require("@ethereumjs/common").default;
const { Chain, Hardfork } = require("@ethereumjs/common");

const amount = web3.utils.toHex(20);
const common = new Common({
  chain: Chain.Ropsten
});
const txData = {
  nonce: amount,
  to: toAddress,
  gasPrice: "0x59acc918",
  gasLimit: "0x210000",
  value: "0x01",
  data: "0x7f7465737432000000000000000000000000000000000000000000000000000000600057",
};

const privateKey = Buffer.from(
  "8119d43463f3d41308b40b1cfb0a19e1bbda6934ca6ee7c95576ff0214c1006e",
  "hex"
);
const transaction = Transaction.fromTxData(txData, { common });
const signedTx = transaction.sign(privateKey);
const serializedTx = signedTx.serialize();
console.log(serializedTx.toString("hex"));
