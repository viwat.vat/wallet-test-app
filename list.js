const Moralis = require("moralis/node");
const Web3 = require("web3");
/* Moralis init code */
const serverUrl = "https://ivihturr51fv.usemoralis.com:2053/server";
const appId = "Ifhsw3PIABBEzNhPZYIFbN3m9bmptTk2er1m0LFJ";

main = async () => {
  Moralis.start({ serverUrl, appId });

  //Get metadata for an array of tokens
  const options = { chain: ["bsc","eth"], addresses: ["0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82", "0xc9849e6fdb743d08faee3e34dd2d1bc69ea11a51","0xdac17f958d2ee523a2206206994597c13d831ec7"] };
  const tokenMetadata = await Moralis.Web3API.token.getTokenMetadata(options);

  console.log(tokenMetadata);
};

main();
