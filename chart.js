const Moralis = require("moralis/node");
const Web3 = require("web3");
/* Moralis init code */
const serverUrl = "https://ivihturr51fv.usemoralis.com:2053/server";
const appId = "Ifhsw3PIABBEzNhPZYIFbN3m9bmptTk2er1m0LFJ";

main = async () => {
  Moralis.start({ serverUrl, appId });

  let dates1 = [
    "2022-01-15",
    "2022-01-16",
    "2022-01-17",
    "2022-01-18",
    "2022-01-19",
    "2022-01-20",
    "2022-01-21",
    "2022-01-22",
    "2022-01-23",
    "2022-01-24",
    "2022-01-25",
  ];
  let blocks1 = await Promise.all(
    dates1.map(
      async (e, i) => await Moralis.Web3API.native.getDateToBlock({chain: "bsc", date: e.date })
    )
  );

  console.log(blocks1);

  let prices1 = await Promise.all(
    blocks1.map(
      async (e, i) =>
        await Moralis.Web3API.token.getTokenPrice({
          chain: "bsc",
          address: "0x101d82428437127bf1608f699cd651e6abf9766e",
          to_block: e.block,
        })
    )
  );

  console.log(prices1);
};

main();
