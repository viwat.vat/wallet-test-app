const web3 = require("@solana/web3.js");
const ed25519 = require("ed25519-hd-key");
const bip39 = require("bip39");
const derivePath = "m/44'/501'/0'/0'";
const path = "m/44'/501'/0'/0'";
const splToken = require("@solana/spl-token");

main = async () => {
  let connection = new web3.Connection(web3.clusterApiUrl("testnet"));
  //from
  const phraseP =
    "warfare luxury science mammal helmet blind cruel screen wood crawl all push";
  const seedP = bip39.mnemonicToSeed(phraseP);
  const derivedSeedP = ed25519.derivePath(
    path,
    (await seedP).toString("hex")
  ).key;

  const from = web3.Keypair.fromSeed(derivedSeedP);
  console.log(from.publicKey.toBase58());

  //to
  const phraseK =
    "dawn ozone grid edit crystal rose sorry develop mosquito organ release verify";
  const seedK = bip39.mnemonicToSeed(phraseK);
  const derivedSeedK = ed25519.derivePath(
    derivePath,
    (await seedK).toString("hex")
  ).key;
  const to = web3.Keypair.fromSeed(derivedSeedK);
  console.log(to.publicKey.toBase58());

  // Construct my token class
  let myMint = new web3.PublicKey(
    "6aHu5c4kNFHtVALeFsc7cReKLHbiuu3FXCvU4vTHhTPG"
  );
  let myToken = new splToken.Token(
    connection,
    myMint,
    splToken.TOKEN_PROGRAM_ID,
    from
  );

  let fromTokenAccount = await myToken.getOrCreateAssociatedAccountInfo(
    from.publicKey
  );

  let toTokenAccount = await myToken.getOrCreateAssociatedAccountInfo(
    to.publicKey
  );

  // Add token transfer instructions to transaction
  let transaction = new web3.Transaction().add(
    splToken.Token.createTransferInstruction(
      splToken.TOKEN_PROGRAM_ID,
      fromTokenAccount.address,
      toTokenAccount.address,
      from.publicKey,
      [],
      2
    )
  );

  console.log(transaction);

  // Sign transaction, broadcast, and confirm
  let signature = await web3.sendAndConfirmTransaction(
    connection,
    transaction,
    [from]
  );
  console.log("SIGNATURE", signature);
  console.log("SUCCESS");
};

main();
