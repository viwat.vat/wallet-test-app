const Web3 = require("web3");
const web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://eth-ropsten.alchemyapi.io/v2/E6hFktNQl6uPhf2_U_C_ReZwhCq76fKC"
  )
);
const ether = require("ethers");

async function main() {
  const myAddress = "0xA878fDB740f1C0BBAf3ab6afDc210f1d8A0bbcf6";

  const nonce = await web3.eth.getTransactionCount(myAddress, "latest");
  const value = ether.utils.parseEther("0.1").toHexString()

  const transaction = {
    to: "0xcF29f6D9C0b9E0F7169c2215c631066aB42E0181",
    value,
    gas: 30000,
    nonce: nonce,
  };

  const signedTx = await web3.eth.accounts.signTransaction(
    transaction,
    "8119d43463f3d41308b40b1cfb0a19e1bbda6934ca6ee7c95576ff0214c1006e"
  );

  web3.eth.sendSignedTransaction(
    signedTx.rawTransaction,
    function (error, hash) {
      if (!error) {
        console.log("🎉 The hash of your transaction is:", hash);
      } else {
        console.log(
          "❗Something went wrong while submitting your transaction:",
          error
        );
      }
    }
  );
}

main();



// sendToken(){
//     let contract = new ether.Contract
// }
