const Web3 = require("web3");
const HDWalletProvider = require("@truffle/hdwallet-provider");
const { InfuraProvider, EtherscanProvider } = require("@ethersproject/providers");

let mnemonic =
  "wire bullet menu exact make spell cabin will step abuse panel armed";
// const provider =
//   "https://mainnet.infura.io/v3/0d6c797dafe5405f8470c2056635b039";

provider = new EtherscanProvider();

// Connect to rinkeby testnet (these are equivalent)
provider = new EtherscanProvider("mainnet");

// {
//   chainId: 4,
//   ensAddress: '0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e',
//   name: 'rinkeby'
// }

const Web3Client = new Web3(new Web3.providers.HttpProvider(provider));

// The minimum ABI required to get the ERC20 Token balance
const abi = require('./busd-abi.json')
const minABI = [
  {
    constant: true,
    inputs: [
      {
        name: "_owner",
        type: "address",
      },
    ],
    name: "balanceOf",
    outputs: [
      {
        name: "balance",
        type: "uint256",
      },
    ],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
];
const tokenAddress = "0x0d8775f648430679a709e98d2b0cb6250d2887ef";
const walletAddress = "0x1cf56Fd8e1567f8d663e54050d7e44643aF970Ce";

const contract = new Web3Client.eth.Contract(abi, tokenAddress);
console.log(contract);

async function getBalance() {
  const result = await contract.methods.balanceOf("0x914232e773f3c90c81350e3e8cc8636932785ca4").call(); // 29803630997051883414242659
  console.log(result);
}

getBalance();
