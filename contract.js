const Web3 = require("web3");
// https://eth-ropsten.alchemyapi.io/v2/E6hFktNQl6uPhf2_U_C_ReZwhCq76fKC
// https://mainnet.infura.io/v3/0d6c797dafe5405f8470c2056635b039
// https://ropsten.infura.io/v3/0d6c797dafe5405f8470c2056635b039
const web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://ropsten.infura.io/v3/0d6c797dafe5405f8470c2056635b039"
  )
);
const ethers = require("ethers");
const abi = require("./abi.json");
const factoryAbi = require("./factory.json")

main = async () => {
  let contractAddress = "0x9c83dCE8CA20E9aAF9D3efc003b2ea62aBC08351";
  let contract = new web3.eth.Contract(abi, contractAddress);

  try {
    const exchangeAddress = await contract.methods
      .getExchange('0xe98fe69713211375ddc798f6d0fb810d6ea2cf0f')
      .call();
    console.log(exchangeAddress);
  } catch (err) {
    console.log(err);
  }
};

main();
