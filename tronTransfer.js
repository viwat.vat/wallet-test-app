const TronWeb = require("tronweb");
const HttpProvider = TronWeb.providers.HttpProvider;
// const fullNode = new HttpProvider("https://api.trongrid.io");
// const fullNode = new HttpProvider("http://192.168.1.162:8090");
// const solidityNode = new HttpProvider("https://api.trongrid.io");
// const eventServer = new HttpProvider("https://api.trongrid.io");
const privateKey =
  "b8ddc1a2a3b4dc1241bcf7bf3fbd51c8a5538fcef5e2f1bae3e73fd337aef1f6";

const fullNode = new HttpProvider("https://api.nileex.io");
const solidityNode = new HttpProvider("https://api.nileex.io");
const eventServer = new HttpProvider("https://api.nileex.io");
const tronWeb = new TronWeb(fullNode, solidityNode, eventServer, privateKey);
const CONTRACT = "TXLAQ63Xg1NAzckPwKHvzw7CSEmLMEqcdj"; // USDT
//address tron base58
const ACCOUNT = "TPrtETZ1f5bGxN5fEXKjbW7VUh2LTiTxVc";

// async function main() {
//     let {
//         transaction,
//         result
//     } = await tronWeb.transactionBuilder.triggerSmartContract(
//         CONTRACT, 'transfer(address,uint256)', {
//             feeLimit: 1000,
//             callValue: 0
//         },
//         [{
//             type: 'address',
//             value: ACCOUNT
//         }, {
//             type: 'uint256',
//             value: 100
//         }]
//     );
//     if (!result.result) {
//         console.error("error:", result);
//         return;
//     }
//     console.log("transaction =>", JSON.stringify(transaction, null, 2));

//     const signature = await tronWeb.trx.sign(transaction.raw_data_hex, privateKey);
//     console.log("Signature:", signature);
//     transaction["signature"] = [signature];

//     const broadcast = await tronWeb.trx.sendRawTransaction(transaction);
//     console.log("result:", broadcast);

//     const {
//         message
//     } = broadcast;
//     if (message) {
//         console.log("Error:", Buffer.from(message, 'hex').toString());
//     }
// }

// main().then(() => {
//         console.log("ok");
//     })
//     .catch((err) => {
//         console.trace(err);
//     });

async function triggerSmartContract() {
  try {
    const options = {
      feeLimit: 1000000,
      callValue: 5000,
    };
    const parameters = [
      { type: "address", value: tronWeb.address.toHex(ACCOUNT) },
      { type: "uint", value: 100 },
    ];
    const issuerAddress = tronWeb.defaultAddress.base58;
    const functionSelector = "testFunction(string,uint)";

    let transactionObject =
      await tronWeb.transactionBuilder.triggerSmartContract(
        CONTRACT,
        functionSelector,
        options,
        parameters,
        tronWeb.address.toHex(issuerAddress)
      );
    // let amount = 1000;
    // const transactionObject = await tronWeb.transactionBuilder.sendTrx(to, amount);
    console.log(transactionObject);

    if (!transactionObject.result || !transactionObject.result.result) {
      return console.error(
        "Unknown error: " + transactionObject.object,
        null,
        2
      );
    }
    // Signing the transaction
    const signedTransaction = await tronWeb.trx.sign(
      transactionObject.transaction
    );

    // console.log("signedTransaction", signedTransaction);
    if (!signedTransaction.signature) {
      return console.error("Transaction was not signed properly");
    }

    // Broadcasting the transaction
    const broadcast = await tronWeb.trx.sendRawTransaction(transactionObject.transaction);
    console.log("broadcast:", broadcast);
  } catch (e) {
    return console.error(e);
  }
}

triggerSmartContract();
