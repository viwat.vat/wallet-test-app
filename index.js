const { EtherscanProvider } = require("@ethersproject/providers");
const ethers = require("ethers");

let mnemonicWallet = ethers.Wallet.fromMnemonic(
  "wire bullet menu exact make spell cabin will step abuse panel armed"
);

provider = new EtherscanProvider("ropsten","1ZJH3P6KX77841Z16TNIKRWDYQ4EY889FZ");
network = ethers.providers.getNetwork("ropsten");
provider = new EtherscanProvider(network);

let wallet = new ethers.Wallet(mnemonicWallet.privateKey);
let walletSigner = wallet.connect(provider);
let contract_address = "";
const abi = require("./busd-abi.json");

provider.getGasPrice().then(async (currentGasPrice) => {
  let gas_price = ethers.utils.hexlify(parseInt(currentGasPrice));
  if (contract_address) {
    // general token send
    let contract = new ethers.Contract(contract_address, abi, walletSigner);

    let totalSupply = await contract.totalSupply();
    let s = ethers.BigNumber.from(totalSupply);
    console.log(s);

    // How many tokens?
    let numberOfTokens = ethers.utils.parseUnits(send_token_amount, 18);
    console.log(`numberOfTokens: ${numberOfTokens}`);

    // Send tokens
    contract.transfer(to_address, numberOfTokens).then((transferResult) => {
      console.dir(transferResult);
      console.log("Send finished!");
    });
  } else {
    const tx = {
      from: "0xA878fDB740f1C0BBAf3ab6afDc210f1d8A0bbcf6",
      to: "0xcF29f6D9C0b9E0F7169c2215c631066aB42E0181",
      value: ethers.utils.parseEther("0.1"),
      nonce: provider.getTransactionCount(
        "0xA878fDB740f1C0BBAf3ab6afDc210f1d8A0bbcf6",
        "latest"
      ),
      gasLimit: ethers.utils.hexlify(0x210000), // 100000
      gasPrice: gas_price,
    };
    try {
      walletSigner.sendTransaction(tx).then((transaction) => {
        console.dir(transaction);
        console.log("Send finished!");
      });
    } catch (error) {
      console.log("failed to send!!",error);
    }
  }
});
