const Web3 = require('web3');
const contractAbi = require('./busd-abi.json')
// // mainnet
// const web3 = new Web3('https://bsc-dataseed1.binance.org:443');

// testnet
const web3 = new Web3('https://data-seed-prebsc-1-s1.binance.org:8545');

const bnbAdd = '0xB8c77482e45F1F44dE1745F52C74426C631bDD52';
const test = new web3.eth.Contract(contractAbi,bnbAdd);

const add_1 = "0x3792cfafeb1c6b572bf360a4ca9Db237CEe74156";
const add_2 = "0xE09b2dA6F270bDEfE7b88D4eD47909186e7D4e2e";
const tx = {
    nonce:1,
    from: add_2,
    to: add_1,
    value: '1000000000000000000',
    gas: 5000000,
    gasPrice: 18e9,
};
const privateKey_add1 =  '0xdc0b16badc30e40393ba0479ea2d56e542d8530a87167254a5a8232aaa3980ec';
const signPromise = web3.eth.signTransaction(tx, privateKey_add1);
web3.eth.getBalance(add_1).then(value=>console.log("address 1", value));
signPromise.then((signedTx) => {
    console.log(signedTx);
    // raw transaction string may be available in .raw or 
    // .rawTransaction depending on which signTransaction
    // function was called
    const sentTx = web3.eth.sendSignedTransaction(signedTx.raw || signedTx.rawTransaction);
    sentTx.on("receipt", receipt => {
      // do something when receipt comes back
      console.log("back")
    });
    sentTx.on("error", err => {
      // do something on transaction error
      console.log("error")
    });
  }).catch((err) => {
    // do something when promise fails
    console.log(err)
  });
// const account = web3.eth.accounts.create();

// console.log(account);

// address: '0x3792cfafeb1c6b572bf360a4ca9Db237CEe74156',
//   privateKey: '0xdc0b16badc30e40393ba0479ea2d56e542d8530a87167254a5a8232aaa3980ec',
//   signTransaction: [Function: signTransaction],
//   sign: [Function: sign],
//   encrypt: [Function: encrypt]

// {
//     address: '0xE09b2dA6F270bDEfE7b88D4eD47909186e7D4e2e',
//     privateKey: '0xeffa6a55ea18bd6e113d5165201a4600dc489206b21e9bc57cfc9b4a607d867a',
//     signTransaction: [Function: signTransaction],
//     sign: [Function: sign],
//     encrypt: [Function: encrypt]
//   }




    // Make a transaction using the promise
    // web3.eth.sendTransaction(tx, function(err, transactionHash) {
    //   if (err) {
    //     console.log(err);
    //     } else {
    //     console.log(transactionHash);
    //    }
    // });

    web3.eth.getBalance(add_2).then(value=>console.log("address 2", value));