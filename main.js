const Moralis = require("moralis/node");
const Web3 = require('web3');
/* Moralis init code */
const serverUrl = "https://ivihturr51fv.usemoralis.com:2053/server";
const appId = "Ifhsw3PIABBEzNhPZYIFbN3m9bmptTk2er1m0LFJ";
Moralis.start({ serverUrl, appId });

const NODE_URL = "https://speedy-nodes-nyc.moralis.io/99e42094b4c3ddc8ad3312bc/eth/ropsten";
const provider = new Web3.providers.HttpProvider(NODE_URL);
const web3 = new Web3(provider);
const abi = require('./abi.json');
/* TODO: Add Moralis Authentication code */

main = async () => {
  //Get token price on PancakeSwap v2 BSC
  const options = {
    address: "0x1f9840a85d5af5bf1d1762f925bdaddc4201f984",
    chain: "eth",
    exchange: "uniswap-v3",
  };
  const price = await Moralis.Web3API.token.getTokenPrice(options);
  const contract = new web3.eth.Contract(abi,'0x1f9840a85d5af5bf1d1762f925bdaddc4201f984');
  const balanceInToken = await contract.methods.balanceOf('0xA878fDB740f1C0BBAf3ab6afDc210f1d8A0bbcf6').call();

  console.log(price);
  const balance = web3.utils.fromWei(balanceInToken);
  console.log(balance);
  console.log(Number(balance)*Number(price.usdPrice));
};

main();
